package com.euedge.coolstuff;

import com.euedge.coolstuff.domain.filter.KosherFilter;
import com.euedge.coolstuff.domain.filter.NameFilter;
import com.euedge.coolstuff.domain.filter.PriceFilter;
import com.euedge.coolstuff.domain.filter.ProductFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CoolStuffShopApplicationTests {

    private static final String HTTP_LOCALHOST = "http://localhost:";

    @LocalServerPort
    private int port;

    private JacksonTester<List<ProductFilter>> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper();
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void loadAllProducts() throws IOException {
        given()
                .baseUri(HTTP_LOCALHOST + port)
                .get("/api/product")
                .then()
                .log().all()
                .and()
                .statusCode(200)
                .and()
                .body("content.name", hasItems("Cool Melon", "Coleslaw", "Chicken Wings", "Beef Jerky", "Sweet Rolls", "Coleslaw", "Chicken Wings", "Beef Jerky"));
    }

    @Test
    public void filterProductsByNameAndPriceAndDefaultSorting() throws IOException {
        List<ProductFilter> filters = new ArrayList<>();
        filters.add(new NameFilter("co"));
        filters.add(new PriceFilter(1D, 10D));
        String filtersAsJson = json.write(filters).getJson();

        given()
                .baseUri(HTTP_LOCALHOST + port)
                .when()
                .contentType(JSON)
                .body(filtersAsJson)
                .post("/api/product/filter")
                .then()
                .log().all()
                .and()
                .statusCode(200)
                .and()
                .body("content.name", hasItems("Cool Melon", "Coleslaw"))
                .body("content.price", hasItems(10F, 3F))
                .extract().body().asString();
    }

    @Test
    public void filterProductsByNameAndPriceAndSortByName() throws IOException {
        List<ProductFilter> filters = new ArrayList<>();
        filters.add(new NameFilter("c"));
        filters.add(new PriceFilter(1D, 10D));
        String filtersAsJson = json.write(filters).getJson();

        JsonPath content = given()
                .baseUri(HTTP_LOCALHOST + port)
                .when()
                .contentType(JSON)
                .body(filtersAsJson)
                .queryParam("sort", "name,DESC")
                .post("/api/product/filter")
                .then()
                .log().all()
                .and()
                .statusCode(200)
                .and()
                .body("content.name", hasItems("Coleslaw", "Chicken Wings", "Cool Melon"))
                .body("content.price", hasItems(10F, 7F, 3F))
                .body("sort.property", hasItems("name"))
                .body("sort.direction", hasItems("DESC"))
                .extract().body().jsonPath();


        //To check the order
        List<String> names = content.get("content.name");
        assertThat(names)
                .hasSize(3)
                .containsExactly("Cool Melon", "Coleslaw", "Chicken Wings");
    }


    @Test
    public void searchProductsAndFilterByKosher() throws IOException {
        List<ProductFilter> filters = new ArrayList<>();
        filters.add(new KosherFilter(false));
        String filtersAsJson = json.write(filters).getJson();

        String searchTerm = "good";

        JsonPath content = given()
                .baseUri(HTTP_LOCALHOST + port)
                .when()
                .contentType(JSON)
                .body(filtersAsJson)
                .post("/api/product/search/" + searchTerm)
                .then()
                .log().all()
                .and()
                .statusCode(200)
                .and()
                .extract().body().jsonPath();

        List<String> names = content.get("content.name");
        assertThat(names)
                .hasSize(1)
                .containsExactly("Coleslaw");

        List<List<String>> comments = content.getList("content.scores.comment");
        assertThat(comments.get(0))
                .containsExactly("Good", "Epic");

        List<Boolean> kosherFields = content.get("content.kosher");
        assertThat(kosherFields)
                .hasSize(1)
                .containsExactly(false);
    }
}

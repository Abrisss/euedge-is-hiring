package com.euedge.coolstuff.domain.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest
public class ProductFilterTest {

    @Autowired
    private JacksonTester<ProductFilter> json;

    @Test
    public void testNameFilterBackAndForthSerialization() throws IOException {
        ProductFilter nameFilter = new NameFilter("ThisIsTheName");

        JsonContent<ProductFilter> jsonProductFilter = json.write(nameFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(nameFilter);
    }

    @Test
    public void testDescriptionFilterBackAndForthSerialization() throws IOException {
        ProductFilter descriptionFilter = new DescriptionFilter("ThisIsTheDescription");

        JsonContent<ProductFilter> jsonProductFilter = json.write(descriptionFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(descriptionFilter);
    }

    @Test
    public void testPriceFilterBackAndForthSerialization() throws IOException {
        ProductFilter descriptionFilter = new PriceFilter(11.10D, 22D);

        JsonContent<ProductFilter> jsonProductFilter = json.write(descriptionFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(descriptionFilter);
    }

    @Test
    public void testProductTagFilterBackAndForthSerialization() throws IOException {
        ProductFilter productTagFilter = new ProductTagFilter("ProductTagFilterNameValue");

        JsonContent<ProductFilter> jsonProductFilter = json.write(productTagFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(productTagFilter);
    }

    @Test
    public void testProductAvailabilityInfoFilterBackAndForthSerialization() throws IOException {
        ProductFilter productTagFilter = new ProductAvailabilityInfoFilter(1, 2, new Date(), new Date());

        JsonContent<ProductFilter> jsonProductFilter = json.write(productTagFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(productTagFilter);
    }

    @Test
    public void testProductScoreFilterBackAndForthSerialization() throws IOException {
        ProductFilter productTagFilter = new ProductScoreFilter(1D, 3D, "CommentValue");

        JsonContent<ProductFilter> jsonProductFilter = json.write(productTagFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(productTagFilter);
    }

    @Test
    public void testCalculatedScoreFilterBackAndForthSerialization() throws IOException {
        ProductFilter productTagFilter = new CalculatedScoreFilter(1D, 3D);

        JsonContent<ProductFilter> jsonProductFilter = json.write(productTagFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(productTagFilter);
    }

    @Test
    public void testKosherFilterBackAndForthSerialization() throws IOException {
        ProductFilter productTagFilter = new KosherFilter(false);

        JsonContent<ProductFilter> jsonProductFilter = json.write(productTagFilter);
        ObjectContent<ProductFilter> deserializedProductFilter = json.parse(jsonProductFilter.getJson());

        assertThat(deserializedProductFilter.getObject()).isEqualTo(productTagFilter);
    }

}

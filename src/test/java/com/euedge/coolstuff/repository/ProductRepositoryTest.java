package com.euedge.coolstuff.repository;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductScore;
import com.euedge.coolstuff.domain.ProductTag;
import com.euedge.coolstuff.domain.filter.*;
import com.euedge.coolstuff.domain.search.FullSearch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {


    @Autowired
    private ProductRepository underTest;


    @Test
    public void findAllUsingNameFilter() {
        //GIVEN
        NameFilter nameFilter = new NameFilter("c");

        //WHEN
        List<Product> result = underTest.findAll(nameFilter);

        //THEN
        assertThat(result)
                .hasSize(6)
                .extracting(Product::getName)
                .containsExactlyInAnyOrder(
                        "Cool Melon",
                        "Coleslaw",
                        "Chicken Wings",
                        "Coleslaw",
                        "Coleslaw",
                        "Chicken Wings");
    }

    @Test
    public void findAllUsingProductTagFilter() {
        //GIVEN
        ProductTagFilter tagFilter = new ProductTagFilter("es");

        //WHEN
        List<Product> result = underTest.findAll(tagFilter);

        //THEN
        assertThat(result)
                .hasSize(1)
                .extracting(product -> product.getProductTag().get(0).getName())
                .containsExactlyInAnyOrder(
                        "vegetables");
    }

    @Test
    public void findAllUsingProductScoreFilter() {
        //GIVEN
        ProductScoreFilter scoreFilter = new ProductScoreFilter(0D, 2D, "p");

        //WHEN
        List<Product> result = underTest.findAll(scoreFilter);

        //THEN
        assertThat(result)
                .hasSize(1)
                .extracting(product ->
                        tuple(product.getScores().get(0).getComment(),
                                product.getScores().get(0).getScore()))
                .containsExactlyInAnyOrder(
                        tuple("PITA", 0.5D));
    }

    @Test
    public void findAllUsingProductAvailabilityInfoFilter() {
        //GIVEN
        ProductAvailabilityInfoFilter productAvailabilityInfoFilter =
                new ProductAvailabilityInfoFilter(5, 50, null, null);

        //WHEN
        List<Product> result = underTest.findAll(productAvailabilityInfoFilter);

        //THEN
        assertThat(result)
                .hasSize(3)
                .extracting(product ->
                        tuple(product.getAvailabilityInfo().getAvailableItemsCount(),
                                product.getAvailabilityInfo().getNextAvailable()))
                .containsExactlyInAnyOrder(
                        tuple(6, null),
                        tuple(42, null),
                        tuple(15, null));
    }

    @Test
    public void findAllUsingPriceFilter() {
        //GIVEN
        PriceFilter priceFilter = new PriceFilter(30D, 80D);

        //WHEN
        List<Product> result = underTest.findAll(priceFilter);

        //THEN
        assertThat(result)
                .hasSize(3)
                .extracting(Product::getPrice)
                .containsExactlyInAnyOrder(32D, 44D, 70D);
    }

    @Test
    public void findAllUsingKosherFilter() {
        //GIVEN
        KosherFilter kosherFilter = new KosherFilter(true);

        //WHEN
        List<Product> result = underTest.findAll(kosherFilter);

        //THEN
        assertThat(result)
                .hasSize(2)
                .extracting(Product::getName)
                .containsExactlyInAnyOrder("Cool Melon", "Sweet Rolls");
    }

    @Test
    public void findAllUsingSearchSimpleCase() {
        //GIVEN
        FullSearch fullSearch = new FullSearch("es");

        //WHEN
        List<Product> result = underTest.findAll(fullSearch);

        //THEN
        assertThat(result)
                .hasSize(3)
                .extracting(product -> tuple(product.getName(), product.getPrice()))
                .containsExactlyInAnyOrder(
                        tuple("Coleslaw", 3D),
                        tuple("Coleslaw", 14D),
                        tuple("Coleslaw", 44D));
    }

    @Test
    public void findAllUsingSearchWithMultiTermAndSingleField() {
        //GIVEN
        FullSearch fullSearch = new FullSearch("es be");

        //WHEN
        List<Product> result = underTest.findAll(fullSearch);

        //THEN
        assertThat(result)
                .hasSize(5)
                .extracting(product -> tuple(product.getName(), product.getPrice()))
                .containsExactlyInAnyOrder(
                        tuple("Coleslaw", 3D),
                        tuple("Coleslaw", 14D),
                        tuple("Coleslaw", 44D),
                        tuple("Beef Jerky", 32D),
                        tuple("Beef Jerky", 70D));
    }

    @Test
    public void findAllUsingSearchWithMultiTermAndMultiFieldSimpleCase() {
        //GIVEN
        FullSearch fullSearch = new FullSearch("es be si");

        //WHEN
        List<Product> result = underTest.findAll(fullSearch);

        //THEN
        assertThat(result)
                .hasSize(7)
                .extracting(product -> tuple(product.getName(), product.getDescription(), product.getPrice()))
                .containsExactlyInAnyOrder(
                        tuple("Coleslaw", "Cool salads", 3D),
                        tuple("Coleslaw", "Cool salads", 14D),
                        tuple("Coleslaw", "Cool salads", 44D),
                        tuple("Beef Jerky", "Moo", 32D),
                        tuple("Beef Jerky", "Moo", 70D),
                        tuple("Sweet Rolls", "Green outside Red inside", 3D),
                        tuple("Cool Melon", "Green outside Red inside", 10D));
    }

    @Test
    public void findAllUsingSearchWithMultiTermAndMultiFieldComplexCase() {
        //GIVEN
        FullSearch fullSearch = new FullSearch("not table");

        //WHEN
        List<Product> result = underTest.findAll(fullSearch);

        //THEN
        assertThat(result)
                .hasSize(2)
                .extracting(product -> tuple(product.getName(), product.getPrice()))
                .containsExactlyInAnyOrder(
                        tuple("Coleslaw", 3D),
                        tuple("Cool Melon", 10D));

        Product coleslaw = result.stream()
                .filter(product -> product.getName().equals("Coleslaw"))
                .findAny().get();
        assertThat(coleslaw.getProductTag())
                .extracting(ProductTag::getName)
                .containsExactlyInAnyOrder("vegetables");

        Product coolMelon = result.stream()
                .filter(product -> product.getName().equals("Cool Melon"))
                .findAny().get();
        assertThat(coolMelon.getScores())
                .extracting(ProductScore::getComment)
                .containsExactlyInAnyOrder(
                        "Not soo bad",
                        "Good start, but...");
    }
}

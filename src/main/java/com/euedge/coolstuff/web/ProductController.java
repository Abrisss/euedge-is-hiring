package com.euedge.coolstuff.web;

import com.euedge.coolstuff.business.ProductService;
import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.filter.ProductFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping
    public Page<Product> loadAllProducts(Pageable pageable) {
        logger.info("Started to load all products!");
        Page<Product> products = productService.loadAllProducts(pageable);
        logger.info("Finished to load all products!");
        return products;
    }

    @RequestMapping(path = "filter", method = RequestMethod.POST)
    public Page<Product> filterProducts(@RequestBody List<ProductFilter> filters, Pageable pageable) {
        logger.info("Started to filter products!");
        logger.debug("Filters: " + filters);
        Page<Product> filteredProducts = productService.filterProducts(filters, pageable);
        logger.info("Finished to filter products!");
        return filteredProducts;
    }

    // TODO: show available products first -> itt csak egy  @SortDefault-ot allitottam be eleinte, de igazabol az is teljesen ertelmetlen, ezert ki is vettem.

    // TODO: helyben logolas helyett aspect bevezetese -> erre nem jutott idom

    @RequestMapping(path = "search/{searchTerm}", method = RequestMethod.POST)
    public Page<Product> search(@PathVariable String searchTerm, @RequestBody List<ProductFilter> filters, Pageable pageable) {
        logger.info("Started to search for products!");
        logger.debug("SearchTerm: " + searchTerm + " filters: " + filters);
        Page<Product> searchedProducts = productService.searchAndFilterProducts(searchTerm, filters, pageable);
        logger.info("Finished to search for products!");
        return searchedProducts;
    }
    // TODO: introduce error handling -> erre nem jutott idom
}

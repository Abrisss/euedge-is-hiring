package com.euedge.coolstuff.domain.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.List;

class PredicateUtil {

    static Predicate inCombinedWithLikePredicate(List<String> listOfValues, Path<String> path, CriteriaBuilder cb) {
        return cb.or(listOfValues.stream()
                .map(s -> cb.like(cb.upper(path), "%" + s.toUpperCase() + "%"))
                .toArray(Predicate[]::new));
    }
}

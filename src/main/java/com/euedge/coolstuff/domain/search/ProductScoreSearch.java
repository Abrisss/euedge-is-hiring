package com.euedge.coolstuff.domain.search;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductScore_;
import com.euedge.coolstuff.domain.Product_;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

import static com.euedge.coolstuff.domain.search.PredicateUtil.inCombinedWithLikePredicate;
import static javax.persistence.criteria.JoinType.LEFT;

@AllArgsConstructor
public class ProductScoreSearch implements Specification<Product> {

    private List<String> searchTermSplitByWhiteSpace;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return commentPredicate(root, cb);
    }

    private Predicate commentPredicate(Root<Product> root, CriteriaBuilder cb) {
        return inCombinedWithLikePredicate(searchTermSplitByWhiteSpace, root.join(Product_.scores, LEFT).get(ProductScore_.comment), cb);
    }
}

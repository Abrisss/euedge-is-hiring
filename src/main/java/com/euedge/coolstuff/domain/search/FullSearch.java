package com.euedge.coolstuff.domain.search;

import com.euedge.coolstuff.domain.Product;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

import static java.util.Arrays.asList;

/*
Nem teljesen jo az implementacio, mivel ahogy a user egyre tobb szot gepel be, egyre tobb sor jelenik meg, mivel OR
kapcsolat van mindig a predikatumok kozott.
Elkezdtem egy Elasticsearch-es megoldast is, de vegul nem tudtam osszekotni a meglevo filterezessel.
De most mar valoszinunek latom, hogy a filterezest is meg lehetett volna oldani elasticsearchel is, es akkor kompatibilis lett volna a ketto.
 */
public class FullSearch implements Specification<Product> {

    private List<String> searchTermSplitByWhiteSpace;

    public FullSearch(String searchTerm) {
        searchTermSplitByWhiteSpace = asList(searchTerm.split("\\s+"));
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        query.distinct(true);
        return cb.or(
                new ProductSearch(searchTermSplitByWhiteSpace).toPredicate(root, query, cb),
                new ProductScoreSearch(searchTermSplitByWhiteSpace).toPredicate(root, query, cb),
                new ProductTagSearch(searchTermSplitByWhiteSpace).toPredicate(root, query, cb));
    }
}

package com.euedge.coolstuff.domain.search;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.Product_;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

import static com.euedge.coolstuff.domain.search.PredicateUtil.inCombinedWithLikePredicate;

@AllArgsConstructor
public class ProductSearch implements Specification<Product> {

    private List<String> searchTermSplitByWhiteSpace;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.or(namePredicate(root, cb), descriptionPredicate(root, cb));
    }

    private Predicate namePredicate(Root<Product> root, CriteriaBuilder cb) {
        return inCombinedWithLikePredicate(searchTermSplitByWhiteSpace, root.get(Product_.name), cb);
    }

    private Predicate descriptionPredicate(Root<Product> root, CriteriaBuilder cb) {
        return inCombinedWithLikePredicate(searchTermSplitByWhiteSpace, root.get(Product_.description), cb);
    }
}

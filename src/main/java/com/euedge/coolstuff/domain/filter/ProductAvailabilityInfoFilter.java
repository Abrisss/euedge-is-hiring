package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductAvailabilityInfo;
import com.euedge.coolstuff.domain.ProductAvailabilityInfo_;
import com.euedge.coolstuff.domain.Product_;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.criteria.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"fromAvailableItemsCount", "toAvailableItemsCount", "fromNextAvailable", "toNextAvailable"})
@ToString
public class ProductAvailabilityInfoFilter implements ProductFilter {
    @JsonProperty
    private Integer fromAvailableItemsCount;

    @JsonProperty
    private Integer toAvailableItemsCount;

    @JsonProperty
    private Date fromNextAvailable;

    @JsonProperty
    private Date toNextAvailable;


    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Join<Product, ProductAvailabilityInfo> join = root.join(Product_.availabilityInfo);
        Predicate availableItemsCountPredicate = determineAvailableItemsCountPredicate(cb, join);
        Predicate nextAvailablePredicate = determineNextAvailablePredicate(cb, join);
        return cb.and(availableItemsCountPredicate, nextAvailablePredicate);
    }

    private Predicate determineNextAvailablePredicate(CriteriaBuilder cb, Join<Product, ProductAvailabilityInfo> join) {
        Predicate nextAvailableIsNullPredicate = cb.isNull(join.get(ProductAvailabilityInfo_.nextAvailable));
        Predicate fromNextAvailablePredicate = cb.greaterThanOrEqualTo(join.get(ProductAvailabilityInfo_.nextAvailable), fromNextAvailable);
        Predicate toNextAvailablePredicate = cb.lessThanOrEqualTo(join.get(ProductAvailabilityInfo_.nextAvailable), toNextAvailable);
        return cb.or(nextAvailableIsNullPredicate, cb.and(fromNextAvailablePredicate, toNextAvailablePredicate));
    }

    private Predicate determineAvailableItemsCountPredicate(CriteriaBuilder cb, Join<Product, ProductAvailabilityInfo> join) {
        Predicate availableItemsCountIsNullPredicate = cb.isNull(join.get(ProductAvailabilityInfo_.availableItemsCount));
        Predicate fromAvailableItemsCountPredicate = cb.greaterThanOrEqualTo(join.get(ProductAvailabilityInfo_.availableItemsCount), fromAvailableItemsCount);
        Predicate toAvailableItemsCountPredicate = cb.lessThanOrEqualTo(join.get(ProductAvailabilityInfo_.availableItemsCount), toAvailableItemsCount);
        return cb.or(availableItemsCountIsNullPredicate, cb.and(fromAvailableItemsCountPredicate, toAvailableItemsCountPredicate));
    }
}

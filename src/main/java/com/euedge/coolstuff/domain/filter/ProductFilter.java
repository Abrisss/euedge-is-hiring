package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.data.jpa.domain.Specification;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@JsonTypeInfo(use = NAME, property = "filter")
@JsonSubTypes({
        @Type(name = "name", value = NameFilter.class),
        @Type(name = "description", value = DescriptionFilter.class),
        @Type(name = "kosher", value = KosherFilter.class),
        @Type(name = "productTag", value = ProductTagFilter.class),
        @Type(name = "productAvailabilityInfoFilter", value = ProductAvailabilityInfoFilter.class),
        @Type(name = "productScoreFilter", value = ProductScoreFilter.class),
        @Type(name = "calculatedScoreFilter", value = CalculatedScoreFilter.class),
        @Type(name = "price", value = PriceFilter.class)})
public interface ProductFilter extends Specification<Product> {
    //TODO: or null criteria filterek
}

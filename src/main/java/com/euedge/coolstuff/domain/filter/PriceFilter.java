package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.Product_;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"fromPrice", "toPrice"})
@ToString
public class PriceFilter implements ProductFilter {
    @JsonProperty
    private Double fromPrice;

    @JsonProperty
    private Double toPrice;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Predicate priceIsNullPredicate = cb.isNull(root.get(Product_.price));
        Predicate fromPricePredicate = cb.greaterThanOrEqualTo(root.get(Product_.price), fromPrice);
        Predicate toPricePredicate = cb.lessThanOrEqualTo(root.get(Product_.price), toPrice);
        return cb.or(priceIsNullPredicate, cb.and(fromPricePredicate, toPricePredicate));
    }
}

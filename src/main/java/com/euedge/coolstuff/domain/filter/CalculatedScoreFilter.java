package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"fromCalculatedScore", "toCalculatedScore"})
@ToString
public class CalculatedScoreFilter implements ProductFilter {
    @JsonProperty
    private Double fromCalculatedScore;

    @JsonProperty
    private Double toCalculatedScore;

    public boolean apply(Product product) {
        return product.getPrice() >= fromCalculatedScore && product.getPrice() <= toCalculatedScore;
    }

    /*
       Mivel nincsen a DB-ben benne a calculatedScore, ezert erre nem lehet filterezni DB szinten.
       Ha pedig a service-be raknam bele a logikat, akkor pedig a pageing adna vissza kevesebb erteket.
       En bevezetnem a db-be is ezt az erteket, es menteskor mindig kiszamolnam, es ugy mentenem le a DB-be.
        */
    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return null;
    }
}

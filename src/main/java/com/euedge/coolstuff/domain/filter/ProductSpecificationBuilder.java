package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.search.FullSearch;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.List;

public class ProductSpecificationBuilder {

    public static Specification<Product> build(List<ProductFilter> filters) {
        if (filters.size() == 0) {
            return null;
        }
        Specification<Product> result = filters.get(0);
        for (int i = 1; i < filters.size(); i++) {
            result = Specifications.where(result).and(filters.get(i));
        }
        return result;
    }

    public static Specification<Product> build(String searchTerm, List<ProductFilter> filters) {
        if (filters.size() == 0 && (searchTerm == null || searchTerm.isEmpty())) {
            return null;
        }

        Specification<Product> result = new FullSearch(searchTerm);
        for (ProductFilter filter : filters) {
            result = Specifications.where(result).and(filter);
        }
        return result;
    }
}

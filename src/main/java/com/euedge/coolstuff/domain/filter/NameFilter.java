package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.Product_;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "name")
@ToString
public class NameFilter implements ProductFilter {
    @JsonProperty
    private String name;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.or(cb.isNull(root.get(Product_.name)), cb.like(cb.upper(root.get(Product_.name)), "%" + name.toUpperCase() + "%"));
    }
}

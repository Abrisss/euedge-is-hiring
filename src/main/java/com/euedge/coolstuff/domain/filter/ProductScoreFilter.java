package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductScore;
import com.euedge.coolstuff.domain.ProductScore_;
import com.euedge.coolstuff.domain.Product_;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.criteria.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"fromScore", "toScore", "comment"})
@ToString
public class ProductScoreFilter implements ProductFilter {

    @JsonProperty
    private Double fromScore;

    @JsonProperty
    private Double toScore;

    @JsonProperty
    private String comment;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ListJoin<Product, ProductScore> join = root.join(Product_.scores);
        Predicate fromScorePredicate = cb.greaterThanOrEqualTo(join.get(ProductScore_.score), fromScore);
        Predicate toScorePredicate = cb.lessThanOrEqualTo(join.get(ProductScore_.score), toScore);
        Predicate commentPredicate = cb.like(cb.upper(join.get(ProductScore_.comment)), "%" + comment.toUpperCase() + "%");
        return cb.and(fromScorePredicate, toScorePredicate, commentPredicate);
    }
}

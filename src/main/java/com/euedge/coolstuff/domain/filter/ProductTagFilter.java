package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductTag;
import com.euedge.coolstuff.domain.ProductTag_;
import com.euedge.coolstuff.domain.Product_;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.criteria.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "productTagName")
@ToString
public class ProductTagFilter implements ProductFilter {
    @JsonProperty
    private String productTagName;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ListJoin<Product, ProductTag> join = root.join(Product_.productTag);
        Predicate productTagNameIsNullPredicate = cb.isNull(join.get(ProductTag_.name));
        Predicate productTagNamLikePredicate = cb.like(cb.upper(join.get(ProductTag_.name)), "%" + productTagName.toUpperCase() + "%");
        return cb.or(productTagNameIsNullPredicate, productTagNamLikePredicate);
    }
}

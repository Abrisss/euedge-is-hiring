package com.euedge.coolstuff.domain;

import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "PRODUCT_AVAILABILITY_INFO")
@ToString(of = {"availableItemsCount", "nextAvailable"})
public class ProductAvailabilityInfo {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "AVAILABLE_ITEMS_COUNT")
    private Integer availableItemsCount;

    @Column(name = "NEXT_AVAILABLE")
    private Date nextAvailable;

    @OneToOne
    private Product product;

    public ProductAvailabilityInfo() {
    }

    public Integer getAvailableItemsCount() {
        return availableItemsCount;
    }

    public void setAvailableItemsCount(Integer availableItemsCount) {
        this.availableItemsCount = availableItemsCount;
    }

    public Date getNextAvailable() {
        return nextAvailable;
    }

    public void setNextAvailable(Date nextAvailable) {
        this.nextAvailable = nextAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}

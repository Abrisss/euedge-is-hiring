package com.euedge.coolstuff.business;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.filter.ProductFilter;
import com.euedge.coolstuff.domain.filter.ProductSpecificationBuilder;
import com.euedge.coolstuff.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public Page<Product> loadAllProducts(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Page<Product> filterProducts(List<ProductFilter> filters, Pageable pageable) {
        return repository.findAll(ProductSpecificationBuilder.build(filters), pageable);
    }

    public Page<Product> searchAndFilterProducts(String searchTerm, List<ProductFilter> filters, Pageable pageable) {
        return repository.findAll(ProductSpecificationBuilder.build(searchTerm, filters), pageable);
    }
}
